<?php
namespace app\core;

use app\core\Router;

class Application
{
    public Router $router;

    public function __construct()
    {
        $this->router = new Router();
    }

    public function run()
    {
        echo $this->router->resolve();
    }

    public static function doAuth($login, $password) {
        if($login == 'admin' and $password == '123') {
            $_SESSION['user_hash'] = sha1($login.':'.$password);
            return true;
        }

        return false;
    }

    public static function checkAuth() {
        if (empty($_SESSION['user_hash'])) return false;

        return $_SESSION['user_hash'] == sha1('admin:123');
    }

    public static function doLogout() {
        unset($_SESSION['user_hash']);
    }
}