<?php
namespace app\core\model;

use app\core\Db;
 
class TaskModel
{
    private $db;

    public function __construct() {
        $this->db = new Db();
    }

    public function getList($page, $num=3, $orderby="date", $orderasc="desc") {
        $orderbys  = array("date", "name", "email",  "status"); 
        $key = array_search($orderby, $orderbys);
        $orderby = $orderbys[$key];

        if (!in_array($orderasc, ['asc', 'desc'])) {
            $orderasc = 'asc';
        }

        $sql = "SELECT * FROM `task` ORDER BY {$orderby} {$orderasc} LIMIT :start, :num";
        $start = ((int)$page-1) * $num;
        $params = [
            'start' => (int)$start,
            'num' => (int)$num,
        ];


        $tasks = $this->db->row($sql, $params);
        return $tasks;
    }
 
    public function getCount() {
        $sql = 'SELECT count(id) FROM `task`';

        $count = $this->db->column($sql);
        return $count;
    }

    public function success($id) {
        $sql = 'UPDATE `task` SET `status` = 1 WHERE `id` = :id';
        $params = [
            'id' => $id,
        ];

        $this->db->query($sql, $params);
    }

    public function editText($task) {
        $sql = 'UPDATE `task` SET `text` = :text, `edited` = 1 WHERE `id` = :id';
        $params = [
            'id' => $task['id'],
            'text' => $task['text'],
        ];

        $this->db->query($sql, $params);
    }

    public function save($task) {
        $sql = 'insert into task(name, email, text) VALUES (:name, :email, :text)';
        $this->db->query($sql, $task);
    }
}
