<?php
namespace app\core;

use app\core\Application;
use app\core\Request;

class View
{
    public $title = 'Title';
    public $h1 = 'H1';

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setH1($h1) {
        $this->h1 = $h1;
    }

    public static function redirect($path) {
        header('Location: '.$path);
        exit;
    }

    public function render_pagination($page, $pagesCount) {
        require APP_ROOT."/core/view/pagination.php";
    }

    public function render($view='', $params=[]) {
        foreach ($params as $key => $value) {
            $$key = $value;
        }

        $h1 = $this->h1;
        $title = $this->title;

        $page = Request::getData('page');
        if (empty($page)) {
            $page = 1;
        }

        $isAuthorized = Application::checkAuth();

        ob_start();

        require_once APP_ROOT."/core/view/header.php";

        if (!empty($view)) {
            if (file_exists(APP_ROOT."/core/view/{$view}.php")) {
                require_once APP_ROOT."/core/view/{$view}.php";
            } else {
                echo '404';
            }
        } else 

        require_once APP_ROOT."/core/view/footer.php";

        echo ob_get_clean();
    }
}