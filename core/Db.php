<?php
namespace app\core;

use PDO;

class Db {
	protected $db;
	
	public function __construct() {
		require_once(APP_ROOT.'/db.php');
		$this->db = new PDO('mysql:host='.$db['host'].';dbname='.$db['name'].'', $db['user'], $db['password']);
	}

	public function query($sql, $params = []) {
		$stmt = $this->db->prepare($sql);
		if (!empty($params)) {
			foreach ($params as $key => $val) {
                if (
                    is_int($val)
                ) {
    				$stmt->bindValue(':'.$key, $val, PDO::PARAM_INT);
                } else {
    				$stmt->bindValue(':'.$key, $val);
                }
			}
		}
		$stmt->execute();
		return $stmt;
	}

	public function row($sql, $params = []) {
		$result = $this->query($sql, $params);
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}

	public function column($sql, $params = []) {
		$result = $this->query($sql, $params);
		return $result->fetchColumn();
	}


    public static function interpolateQuery($query, $params) {
        $keys = array();
        $values = $params;

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }

            if (is_array($value))
                $values[$key] = implode(',', $value);

            if (is_null($value))
                $values[$key] = 'NULL';
        }
        // Walk the array to see if we can add single-quotes to strings
        array_walk($values, function(&$v, $k) {if (!is_numeric($v) && $v!="NULL") $v = "\'".$v."\'";});

        $query = preg_replace($keys, $values, $query, 1, $count);

        return $query;
    }
}