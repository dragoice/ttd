<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$title?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="/main.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><?=$h1?></a>
    </div>
    <ul class="nav navbar-nav navbar-right">
        <?php if ($isAuthorized) {?>
            <li><a href="javascript:;"> Hello admin!</a></li>
            <li><a href="/logout/"> logout</a></li>
        <?php } else {?>
                <li><a href="/login/"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <?php } ?>
    </ul>
  </div>
</nav>


            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
