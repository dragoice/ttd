<?php
define('APP_ROOT', dirname(__DIR__));
require APP_ROOT . '/vendor/autoload.php';

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use app\core\Application;
use app\core\Request;
use app\core\View;

use app\core\controller\TaskController;

$app = new Application();

$app->router->get('/', function () {
    $taskC = new TaskController();

    $page = Request::getData('page');
    if (empty($page)) {
        $page = 1;
    }

    $sort = Request::getData('sort');
    if (!empty($sort)) {
        if ($sort == $_SESSION['sortingby']) {
            $_SESSION['sortingasc'] = ($_SESSION['sortingasc'] == 'asc') ? 'desc' : 'asc';
        } else {
            $_SESSION['sortingby'] = $sort;
            $_SESSION['sortingasc'] = 'asc';
        }
        $page = 1;
    }

    $taskC->list($page);
});

$app->router->post('/saveTask', function () {
    $taskC = new TaskController();

    $task = Request::getData('task');

    if(empty($task)) {
        return '404';
    }

    $taskC->add($task);

    View::redirect('/');
});

$app->router->post('/login/', function () {
    $loginData = Request::getData('login');

    if(!empty($loginData['login']) && !empty($loginData['password'])) {
        $result = Application::doAuth($loginData['login'], $loginData['password']);
        if(!$result) {
            $view = new View();
            $view->setTitle('Login page');
            $view->setH1('Login page');
            $loginData['error'] = 'loginorpassword';
            $view->render('login', $loginData);
        } else {
            View::redirect('/');
        }
    }
});

$app->router->get('/login/', function () {
    $view = new View();
    $view->setTitle('Login page');
    $view->setH1('Login page');
    $view->render('login', ['error' => '']);
});

$app->router->get('/logout/', function () {
    Application::doLogout();
    View::redirect('/');
});

$app->router->get('/success/', function () {
    $isAuthorized = Application::checkAuth();
    if ($isAuthorized) {
        $taskC = new TaskController();

        $id = Request::getData('id');

        if(empty($id)) {
            return '404';
        }

        $taskC->success($id);
    }
    View::redirect('/');
});

$app->router->post('/editText/', function () {
    $isAuthorized = Application::checkAuth();
    if ($isAuthorized) {
        $taskC = new TaskController();

        $task = Request::getData('task');

        if(empty($task)) {
            return '404';
        }

        $taskC->editText($task);
    }
    View::redirect('/');
});

$app->run();
