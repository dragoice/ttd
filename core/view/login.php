﻿<?php
$haserror = '';
if ($error == 'loginorpassword') {
    $haserror = 'has-error';
}
?>
<div class="col-sm-4 form-group <?=$haserror?>">
    <h2 class="h3 mb-3 font-weight-normal">Please login</h2>
    <form class="form-signin" method="post" action="/login/">
      <label for="inputLogin" class="sr-only">Login</label>
      <input name="login[login]" type="text" value="<?=empty($login) ? '' : $login ?>" id="inputLogin" class="form-control" placeholder="Login" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input name="login[password]" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
      <?php if(!empty($haserror)) { ?>
      <span class="help-block">Wrong password or login</span>
      <? } ?>
    </form>
</div>