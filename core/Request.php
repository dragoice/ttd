<?php
namespace app\core;

class Request
{
    public static function getPath() {
        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');

        if ($position === false) {
            return $path;
        }

        return substr($path, 0, $position);
    }

    public static function getData($parametr) {
        $result = NULL;
        if(!empty($_POST[$parametr])) {
            $result = $_POST[$parametr];
        } else if(!empty($_GET[$parametr])) {
            $result = $_GET[$parametr];
        }
        return $result;
    }

    public static function getMethod() {
        if (empty($_SERVER['REQUEST_METHOD'])) {
            $_SERVER['REQUEST_METHOD'] = 'GET';
        }
        return strtolower($_SERVER['REQUEST_METHOD']);
    }
}
