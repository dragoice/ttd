<?php
namespace app\core;

use app\core\Request;
  
class Router
{
    protected array $routes = [];
  
    public function get($path, $callback)
    {
  	    $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback)
    {
  	    $this->routes['post'][$path] = $callback;
    }

    public function resolve()
    {
        $path = Request::getPath();
        $method = Request::getMethod();
        $callback = $this->routes[$method][$path] ?? false;

        if ($callback === false) {
            return "404";
        }

        return call_user_func($callback);
    }
}
