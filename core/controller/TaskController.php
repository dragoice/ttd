<?php
namespace app\core\controller;
 
use app\core\model\TaskModel;
use app\core\Controller;
use app\core\View;
 
class TaskController 
{
    public function add($data) {
        if (empty($data['name']) || empty($data['email']) || empty($data['text'])) {
            return false;
        }

        $taskM = new TaskModel();
        $taskM->save($data);
    }

    public function success($id) {
        $taskM = new TaskModel();
        $taskM->success($id);
    }

    public function editText($task) {
        $taskM = new TaskModel();
        $taskM->editText($task);
    }

    public function list($page=1) {

        $itemsPerPage = 3;

        if (empty($_SESSION['sortingby'])) {
            $_SESSION['sortingby'] = 'date';
        }

        if (empty($_SESSION['sortingasc'])) {
            $_SESSION['sortingasc'] = 'desc';
        }

        $view = new View();
        $view->setTitle('TaskToDo Free for all!!');
        $view->setH1('TaskToDo Free for all!!');

        $taskM = new TaskModel();

        $data = [];

        $sort_field = 'date';
        $sort_asc = 'desc';

        $data['tasks'] = $taskM->getList($page, $itemsPerPage, $_SESSION['sortingby'], $_SESSION['sortingasc']);

        $data['sorting'] = ['date', 'name', 'email', 'status'];

        $data['sortingby'] = $_SESSION['sortingby'];

        $tasksCount = $taskM->getCount();
        $data['pagesCount'] = ceil($tasksCount / $itemsPerPage);

        $view->render('list', $data);
    }
}
