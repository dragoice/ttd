<div class="row">
        <h4>Tasks:</h4>

<div class="col-sm-12">
  <ul class="pagination">
    <?php $this->render_pagination($page, $pagesCount) ?>
  </ul>
</div>

<h4>Order:</h4>
<ul class="nav nav-pills">
    <?php foreach ($sorting as $sort) { ?>
        <?php $class = ''; if ($sort == $sortingby) {$class = 'active';} ?>
        <li class="<?=$class?>"><a href="/?sort=<?=$sort?>"><?=$sort?></a></li>
    <?php } ?>
</ul>

        <? foreach ($tasks as $task) { ?>
        <div class="col-sm-10">
          <h4>
                <?=htmlspecialchars($task['name'])?> <?=htmlspecialchars($task['email'])?>  <small> <?=$task['date']?></small> 
                <?php if ($task['status'] != '1') {?>
<span class="label label-danger">Accepted</span>
                <?php } else { ?>
<span class="label label-success">Done</span>
                <?php } ?>
                <?php if ($task['edited'] == '1') {?>
<span class="label label-warning">Edited by admin</span>
                <?php } ?>
          </h4>

        <?php if (!$isAuthorized) {?>
          <p><?=htmlspecialchars($task['text'])?></p>
        <?php } else { ?>

<form role="form" action="/editText/" method="post">
<div class="input-group">
    <input type="hidden" name="task[id]" value="<?=$task['id']?>">
    <textarea name="task[text]" class="form-control custom-control" rows="3" style="resize:none"><?=$task['text']?></textarea> <span class="input-group-btn">
    <button type="submit" class="btn btn-primary">Save</button>
    </span>
</div>
</form>
        <?php }  ?>

          <br>

        <?php if ($isAuthorized && $task['status'] != '1') {?>
          <a href="/success/?id=<?=$task['id']?>" class="btn btn-success">Set Done</a>
        <? } ?>

        </div>
        <? } ?>


<div class="col-sm-12">
  <ul class="pagination">
    <?php $this->render_pagination($page, $pagesCount) ?>
  </ul>
</div>

        <div class="col-sm-4">
            <h4>Add Task:</h4>
            <form role="form" action="saveTask" method="post">
            <div class="form-group">
              <label for="name">Your name:</label>
              <input id="name" type="text" class="form-control" name="task[name]" required="" />
            </div>

            <div class="form-group">
              <label for="email">Email:</label>
              <input id="email" type="email" class="form-control" name="task[email]" required="" />
            </div>

            <div class="form-group">
              <label for="text">Task text:</label>
              <textarea id="text" class="form-control" name="task[text]" rows="3" required=""></textarea>
            </div>

            <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>

</div>
