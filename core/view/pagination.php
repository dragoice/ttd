﻿<div class="col-sm-12">
  <ul class="pagination">
    <?php for ($pageNum = 1; $pageNum <= $pagesCount; $pageNum++) { ?>
        <?php $class = ''; if ($page == $pageNum) {$class = 'active';} ?>
        <li class="<?=$class?>"><a href="/?page=<?=$pageNum?>"><?= $pageNum ?></a></li>
    <?php } ?>
  </ul>
</div>
